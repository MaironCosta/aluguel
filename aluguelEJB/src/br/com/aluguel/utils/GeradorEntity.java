package br.com.aluguel.utils;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import br.com.aluguel.exception.ValidacaoException;
import br.com.aluguel.sequencial.business.SequencialBusiness;
import br.com.aluguel.sequencial.business.SequencialBusinessFacade;
import br.com.aluguel.sequencial.entity.Sequencial;
import br.com.aluguel.sequencial.entity.TipoSequencial;

public class GeradorEntity {

	public GeradorEntity() {
		// TODO Auto-generated constructor stub
	}
	
	public static void gerarSequenciais () {
		
		List<Sequencial> sequencials = new ArrayList<Sequencial>();
		
		int anoAtual = LocalDate.now().getYear();
		
		sequencials.add(new Sequencial(0, anoAtual, TipoSequencial.NUMEROCONTRATO));
		sequencials.add(new Sequencial(0, anoAtual, TipoSequencial.REGISTROCLIENTEFISICO));
		sequencials.add(new Sequencial(0, anoAtual, TipoSequencial.REGISTROCLIENTEJURIDICO));
		sequencials.add(new Sequencial(0, anoAtual, TipoSequencial.REGISTROFORNECEDOR));
		sequencials.add(new Sequencial(0, anoAtual, TipoSequencial.REGISTROFUNCIONARIO));
		
		SequencialBusinessFacade sequencialBusinessFacade;
		
		for (Sequencial sequencial : sequencials) {
			
			sequencialBusinessFacade = new SequencialBusiness();
			
			try {
				
				sequencialBusinessFacade.persist(sequencial);
				
			} catch (IllegalArgumentException | ValidacaoException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}

}
