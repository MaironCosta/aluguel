package br.com.aluguel.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class UtilsDate {

	//		Logger log = Logger.getLogger(UtilsDate.class);	

	public UtilsDate() {
		// TODO Auto-generated constructor stub
	}
	
	public static Integer diferencaMes(Calendar dataInicial, Calendar dataFim) {

		if (dataInicial == null || dataFim == null) {
			return null;
		}

		int diferenca = 0;

		dataInicial.set(Calendar.MILLISECOND, 0);  
		dataInicial.set(Calendar.SECOND, 0);  
		dataInicial.set(Calendar.MINUTE, 0);  
		dataInicial.set(Calendar.HOUR_OF_DAY, 0);

		dataFim.set(Calendar.MILLISECOND, 0);  
		dataFim.set(Calendar.SECOND, 0);  
		dataFim.set(Calendar.MINUTE, 0);  
		dataFim.set(Calendar.HOUR_OF_DAY, 0);

		while (dataInicial.before(dataFim)) {

			dataInicial.add(Calendar.MONTH, 1);
			diferenca++;

		}	

		//		new UtilsDate().log.info("Diferenca mes: " + diferenca);

		return diferenca;
	}

	public static String montarDataMes (String data) {

		/*if (Valida.isEmpty(data)) {
				return null;
			}
		 */
		StringBuilder d = new StringBuilder("01/");
		d.append(data.substring(0, 2));
		d.append(data.substring(2, 7));

		return d.toString();
	}

	public static Calendar stringToCalendar (String data) {

		/*if (Valida.isEmpty(data)) {
				return null;
			}*/

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		Calendar d = Calendar.getInstance();
		try {

			d.setTime(sdf.parse(data));

		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	

		return d;
	}

	public static int getIdade(Calendar dataNascimento, Calendar dataBaseCalculo) {

		if (dataNascimento == null || dataBaseCalculo == null) {

			return 0;

		}

		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");

		try {

			dataNascimento.setTime(UtilsDate.stringToCalendar(sdf.format(dataNascimento.getTime())).getTime());
			dataBaseCalculo.setTime(UtilsDate.stringToCalendar(sdf.format(dataBaseCalculo.getTime())).getTime());

		} catch (Exception e) {
			// TODO Auto-generated catch block
			return 0;
		}		

		int idade = dataBaseCalculo.get(Calendar.YEAR) - dataNascimento.get(Calendar.YEAR);
		if (dataBaseCalculo.get(Calendar.YEAR) < dataNascimento.get(Calendar.YEAR)) {

			idade--;

		} else if (dataBaseCalculo.get(Calendar.MONTH) == dataNascimento.get(Calendar.MONTH)) {

			if (dataBaseCalculo.get(Calendar.DAY_OF_MONTH) < dataNascimento.get(Calendar.DAY_OF_MONTH)) {
				--idade;
			}

		}

		return idade;

	}

}
