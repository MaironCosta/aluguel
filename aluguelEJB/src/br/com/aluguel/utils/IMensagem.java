package br.com.aluguel.utils;

public interface IMensagem {	
	
	String CADASTROSUCESSO = "Cadastro Realizado com Sucesso!";
	String CADASTRONAOREALIZADO = "Cadastro n&atilde;o Realizado.";	
	
	String UPDATESUCESSO = "Atualizado com Sucesso!";
	String UPDATENAOREALIZADO = "Atualiza&ccedil;&atilde;o n&atilde;o efetuada.";
	
	String DELETESUCESSO = "Excluido com Sucesso!";
	String DELETENAOREALIZADO = "Remo&ccedil;&atilde;o n&atilde;o efetuada.";
	
	String ERROTRANSACAO = "ERRO.";
	String CONSULTASEMRESULTADO = "Consulta sem resultado.";
	
	String OBJECTOINVALIDO = "Objeto inv&aacute;lido";
	String SUCESSO = "Realizado com Sucesso";
	
}
