package br.com.aluguel.utils;

import java.util.Locale;

public interface ZoneIdLocales {

//	ZoneId ZONE_ID_SAO_PAULO_BR = ZoneId.of("America/Sao_Paulo");
//	ZoneId ZONE_ID_PARIS_FR = ZoneId.of("Europe/Paris");
	
	Locale LOCALE_ID_SAO_PAULO_BR = new Locale("America/Sao_Paulo");
	
}
