package br.com.aluguel.utils;

public final class ServiceLocator {

	public ServiceLocator() {
		// TODO Auto-generated constructor stub
	}	
	
//	static final Logger log = Logger.getLogger(ServiceLocator.class);	
	
    private static final String EJB_INFRAESTRUCTURE_LOOKUP = "Não foi possível estabelecer comunicação com o módulo EJB.";  
  
    // inicializa o contexto JDNI local  
    /*  private static InitialContext ctx;  
  
    static {  
    	
        try {
        	
            ctx = new InitialContext();  
            
        } catch (NamingException e) {  
         //   throw new InfraestructureException(EJB_INFRAESTRUCTURE_LOOKUP);  
        	log.info(EJB_INFRAESTRUCTURE_LOOKUP);
        	e.printStackTrace();
        } 
        
    }  */
  
    /** 
     * Retorna uma instância do EJB a partir da sua interface remota/local. 
     *  
     * @param <T> Tipo da interface remota. 
     * @param clazz Interface remota para fazer a busca. 
     * @return Instância do EJB remoto. 
     */ /* 
    public static IBusinessFacade<?> lookup(Class<?> clazz) {  
    	
        final StringBuilder name = new StringBuilder(60);  
        name.append("java:global/cob/cob-ejb/");  
        name.append(clazz.getSimpleName().replaceAll("Remote|Local", "Bean"));  
        name.append("!");  
        name.append(clazz.getName());  
  
        try {  
        //	log.info("looking for EJB {} by {}", clazz, name);  

        	log.info("looking for EJB {} by {}");  
        	
            return clazz.cast(ctx.lookup(name.toString()));  
  
        } catch (NamingException e) {  
            //   throw new InfraestructureException(EJB_INFRAESTRUCTURE_LOOKUP);  
           	log.info(EJB_INFRAESTRUCTURE_LOOKUP);
           	e.printStackTrace();
           }  
    }  */
    
    
    public static String lookup(Class<?> clazz) {  
    	
        final StringBuilder name = new StringBuilder(60);  
        name.append("java:global/petshopEJB_EAR/petshopEJB//");  
     //   name.append("ejb:sisgedEAR/sisgedEJB//"); 
   //     name.append(clazz.getSimpleName().replaceAll("Remote|Local", "Bean"));  
        name.append(clazz.getSimpleName().replaceAll("Facade", "").replaceFirst("I", ""));  
        name.append("!");  
        name.append(clazz.getName());  
  
        try {  
        //	log.info("looking for EJB {} by {}", clazz, name);  

    //    	log.info("looking for EJB {} by {} " + clazz + " __ " + name);  
        	
  
        } catch (Exception e) {  
            //   throw new InfraestructureException(EJB_INFRAESTRUCTURE_LOOKUP);  
    //       	log.info(EJB_INFRAESTRUCTURE_LOOKUP);
           	e.printStackTrace();
        }  
        
        return name.toString();  
    }  

}
