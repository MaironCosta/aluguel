package br.com.aluguel.utils;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Collection;
import java.util.GregorianCalendar;
import java.util.InputMismatchException;
import java.util.TimeZone;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.com.aluguel.cadastro.entity.CadEndereco;
import br.com.aluguel.cadastro.entity.CadLogin;

public class Valida {

	static TimeZone COUNTRY_TIMEZONE = TimeZone.getTimeZone("America/Sao_Paulo");		
	
	public static boolean isEmpty(Collection<?> list){
		
		if(list == null)
			return true;
		else if(list.isEmpty()) 
			return true;
		else if(list.size() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(String string){
		
		if(string == null)
			return true;
		else if ("".equals(string.trim()))
			return true;
		else if (string.length() < 1)
			return true;
		
		return false;
	}
	
	public static boolean isEmpty(Calendar date){
		
		if(date == null)
			return true;

		return false;
	}	
	
	public static boolean isCompleteEndereco(CadEndereco e){
		
		if(Valida.isEmpty(e.getLogradouro())) 
			return false;
		else if(Valida.isEmpty(e.getMunicipio())) 
			return false;
		else if(Valida.isEmpty(e.getCep())) 
			return false;
		else if(Valida.isEmpty(e.getBairro())) 
			return false;		
		else if(Valida.isEmpty(e.getNumero()))
			return false;
	//	else if (Valida.isEmpty(e.getCidade()))
	//		return false;
		else if (e.getUf() == null)
			return false;
		
		return true;
	}
	
	public static boolean isEmptyEndereco(CadEndereco e){
		
		if(!Valida.isEmpty(e.getLogradouro())) 
			return false;
		else if(!Valida.isEmpty(e.getMunicipio())) 
			return false;
		else if(!Valida.isEmpty(e.getCep()))  
			return false;
		else if(!Valida.isEmpty(e.getBairro())) 
			return false;
		
		return true;
	}
	
	public static boolean validaLogin(CadLogin l){
		
	/*	if(Valida.isEmpty(l.getSenha()) || Valida.isEmpty(l.getPessoa().getEmail()))
			return false; 			
		else if(l.getPessoa().getEmail() == l.getSenha()) 
			return false;
		else if(l.getSenha().length() < 4 || l.getSenha().length() > 8)		
			return false;			
		else if(l.getPessoa().getEmail().length() < 3)
			return false;*/
			
		return true;
		
	}
	
	public static boolean validaNome(String name){
		
		if(Valida.isEmpty(name))
			return false;
		if(name.length() < 3)
			return false;
		else if(name.length() > 60)
			return false;
		
		return true;
	}
	
	public static boolean isDataMaiorQueHoje(Calendar date){
		
		if(date == null)
			return false;
	
		Calendar now = GregorianCalendar.getInstance(COUNTRY_TIMEZONE);

		if(date.getTime().after(now.getTime())) 
			return true;
		
		return false;
	}
	
	public static boolean isDataAnteriorHoje(Calendar date){
		
		if(date == null)
			return false;
	
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		
		Calendar now = GregorianCalendar.getInstance(COUNTRY_TIMEZONE);		

		try {
			
			now.setTime(sdf.parse(sdf.format(now.getTime())));
			date.setTime(sdf.parse(sdf.format(date.getTime())));
			
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		
		System.out.println("NOW: " + sdf.format(now.getTime()));
		System.out.println("DATE: " + sdf.format(date.getTime()));

		if(date.before(now)) 
			return true;
		
		return false;
	}

	/**
	 * Verifica se ï¿½ Domingo ou Segunda-feira
	 * 
	 * @author Mairon
	 * @since 01/03/2013
	 * 
	 * */
	public static boolean isFinalDeSemana (Calendar data) {

		System.out.println("Dia da Semana: " + data.get(Calendar.DAY_OF_WEEK));
		if (data.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY)
			return true;		
		else if (data.get(Calendar.DAY_OF_WEEK) == Calendar.MONDAY)
			return true;
		
		return false;
	}
	
	public static boolean validarCPF(String cpf){

		if (cpf == null) {
			
			return false;
			
		}
		
		cpf = cpf.replace(".", "").replace("-", "");
		System.out.println("CPF: " + cpf);
		
		for (int i = 0; i <= (cpf.length() - 1); i++) {

			try {
				
				Integer.valueOf(String.valueOf(cpf.charAt(i)));
				
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
				return false;
			}
					
		}
		
		if ("11111111111".equals(cpf) || "22222222222".equals(cpf) || "33333333333".equals(cpf) 
			|| "44444444444".equals(cpf) || "55555555555".equals(cpf) || "66666666666".equals(cpf)
            || "77777777777".equals(cpf) || "88888888888".equals(cpf) || "99999999999".equals(cpf) 
            || "00000000000".equals(cpf)) {
			
			return false;
			
		}

		int somaPrimeiroDigito = 0;
		int multiplicador = 10;

		for (int i = 0; i <= (cpf.length() -3); i++) {

			somaPrimeiroDigito += (Integer.valueOf(String.valueOf(cpf.charAt(i))) * multiplicador);
			multiplicador--;			
		}
		
		int primeiroDigito = (somaPrimeiroDigito % 11 == 0 || somaPrimeiroDigito % 11 == 1)?
				(somaPrimeiroDigito % 11): 11 - (somaPrimeiroDigito % 11);

		// CALCULAR O SEGUNDO DIGITO

		int somaSegundoDigito = 0;
		multiplicador = 10;

		for (int i = 1; i <= (cpf.length() -2); i++) {

			somaSegundoDigito += (Integer.valueOf(String.valueOf(cpf.charAt(i))) * multiplicador);
	//		System.out.println("Caracter: " + cpf.charAt(i) + " Multiplicador: " + multiplicador + " somaPrimeiroDigito: " + somaSegundoDigito);
			multiplicador--;					
	//		somaSegundoDigito += (multiplicador == 2)? (primeiroDigito * multiplicador): 0;			
			
		}

		int segundoDigito = (somaSegundoDigito % 11 == 0 || somaSegundoDigito % 11 == 1)?
				(somaSegundoDigito % 11):
					11 -(somaSegundoDigito % 11);

		System.out.println("Primeiro Digito: " + primeiroDigito + " Segundo Digito: " + segundoDigito);
		int cpfVerificador01 = Integer.valueOf(String.valueOf(cpf.subSequence((cpf.length()-2), (cpf.length()-1))));
		int cpfVerificador02 = Integer.valueOf(String.valueOf(cpf.subSequence((cpf.length()-1), cpf.length()))); 

		if ((primeiroDigito == 0 || primeiroDigito == 1 || primeiroDigito == cpfVerificador01) &&
				(segundoDigito == 0 || segundoDigito == 1 || segundoDigito == cpfVerificador02)) {
			
			return true;
			
		}
		
		return false;
	}
	
	public static boolean isEmail(String email) {  
		
	    Pattern pattern = Pattern.compile("^[\\w-]+(\\.[\\w-]+)*@([\\w-]+\\.)+[a-zA-Z]{2,7}$");   
	    Matcher matcher = pattern.matcher(email);   
	    
	    return matcher.find();   
	} 
	
/*	
 *   javascript
 * 
 * function IsEmail(email){

	    var exclude=/[^@-.w]|^[_@.-]|[._-]{2}|[@.]{2}|(@)[^@]*1/;
	    var check=/@[w-]+./;
	    var checkend=/.[a-zA-Z]{2,3}$/;
	    if(((email.search(exclude) != -1)||(email.search(check)) == -1)||(email.search(checkend) == -1)){return false;}
	    else {return true;}
	}*/

	public static boolean validarCNPJ(String CNPJ) { 
		// considera-se erro CNPJ's formados por uma sequencia de numeros iguais 
		if (CNPJ.equals("00000000000000") || 
				CNPJ.equals("11111111111111") || 
				CNPJ.equals("22222222222222") || 
				CNPJ.equals("33333333333333") || 
				CNPJ.equals("44444444444444") || 
				CNPJ.equals("55555555555555") || 
				CNPJ.equals("66666666666666") || 
				CNPJ.equals("77777777777777") || 
				CNPJ.equals("88888888888888") || 
				CNPJ.equals("99999999999999") || 
				(CNPJ.length() != 14)) {

			return(false); 
		}
		char dig13, dig14; 
		int sm, i, r, num, peso; 
		// "try" - protege o código para eventuais erros de conversao de tipo (int) 
		try { // Calculo do 1o. Digito Verificador 
			sm = 0; 
			peso = 2; 

			for (i=11; i>=0; i--) { 
				// converte o i-ésimo caractere do CNPJ em um número: // por exemplo, transforma o caractere '0' no inteiro 0 
				// (48 eh a posição de '0' na tabela ASCII) 
				num = (int)(CNPJ.charAt(i) - 48); 
				sm = sm + (num * peso); 
				peso = peso + 1; 
				if (peso == 10) { 

					peso = 2; 

				} 

			} 

			r = sm % 11; 
			if ((r == 0) || (r == 1)){ 

				dig13 = '0'; 

			} else {

				dig13 = (char)((11-r) + 48); 

			}// Calculo do 2o. Digito Verificador 

			sm = 0; 
			peso = 2; 

			for (i=12; i>=0; i--) {

				num = (int)(CNPJ.charAt(i)- 48); 
				sm = sm + (num * peso); 
				peso = peso + 1; 
				if (peso == 10) { 

					peso = 2; 

				} 

			} 

			r = sm % 11; 

			if ((r == 0) || (r == 1)) { 

				dig14 = '0'; 

			} else { 

				dig14 = (char)((11-r) + 48); 

			}// Verifica se os dígitos calculados conferem com os dígitos informados. 

			if ((dig13 == CNPJ.charAt(12)) && (dig14 == CNPJ.charAt(13))) { 

				return(true); 

			} else { 

				return(false); 

			} 

		} catch (InputMismatchException erro) { 

			return(false); 

		} 
		
	} 
	
	public static boolean isDataValida (Calendar data) {
		
		if (data == null) {
			return false;
		}
		
		if (data.get(Calendar.MONTH) < 0 
			|| data.get(Calendar.MONTH) > 11
			|| data.get(Calendar.DAY_OF_MONTH) < 0 
			|| data.get(Calendar.DAY_OF_MONTH) > 31
			|| (data.get(Calendar.MONTH) == 1 && data.get(Calendar.DAY_OF_MONTH) > 29)) {
			
			return false;
			
		}
				
		return true;
	}

	//	Leia mais em: Validando o CNPJ em uma Aplicação Java 
    // http://www.devmedia.com.br/validando-o-cnpj-em-uma-aplicacao-java/22374#ixzz2kq693QNJ
	
	public static void main(String[] args) {		
		
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
		Calendar calendar = Calendar.getInstance();
		calendar.set(Calendar.DAY_OF_MONTH, 33);
		
		System.out.println("Data atual: " + sdf.format(calendar.getTime()) + " --- " + Valida.isDataValida(calendar));
		
	}
	
}
