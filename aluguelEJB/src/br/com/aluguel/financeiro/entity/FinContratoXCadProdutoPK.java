package br.com.aluguel.financeiro.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class FinContratoXCadProdutoPK implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Column(name = "FK_CODIGO_CONTRATO",nullable=false)
	private long contrato;
	
	@Column(name = "FK_CODIGO_PRODUTO",nullable=false)
	private long produto;

	public FinContratoXCadProdutoPK() {
		// TODO Auto-generated constructor stub
	}

	public long getContrato() {
		return contrato;
	}

	public void setContrato(long contrato) {
		this.contrato = contrato;
	}

	public long getProduto() {
		return produto;
	}

	public void setProduto(long produto) {
		this.produto = produto;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (contrato ^ (contrato >>> 32));
		result = prime * result + (int) (produto ^ (produto >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinContratoXCadProdutoPK other = (FinContratoXCadProdutoPK) obj;
		if (contrato != other.contrato)
			return false;
		if (produto != other.produto)
			return false;
		return true;
	}

}
