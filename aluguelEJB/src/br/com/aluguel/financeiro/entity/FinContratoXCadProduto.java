package br.com.aluguel.financeiro.entity;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "FIN_CONTRATO_PRODUTO")
public class FinContratoXCadProduto implements Serializable{
	 
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@EmbeddedId        
	private FinContratoXCadProdutoPK codigo; 
	
	@Column(name = "QUANTIDADE_PEDIDO")
	private BigDecimal quantidadePedido;
	
	@Column(name = "VALOR_TOTAL_PRODUTO")
	private BigDecimal valorTotalProduto;
	
	public FinContratoXCadProduto() {
		// TODO Auto-generated constructor stub
	}

	public FinContratoXCadProdutoPK getCodigo() {
		return codigo;
	}

	public void setCodigo(FinContratoXCadProdutoPK codigo) {
		this.codigo = codigo;
	}

	public BigDecimal getQuantidadePedido() {
		return quantidadePedido;
	}

	public void setQuantidadePedido(BigDecimal quantidadePedido) {
		this.quantidadePedido = quantidadePedido;
	}

	public BigDecimal getValorTotalProduto() {
		return valorTotalProduto;
	}

	public void setValorTotalProduto(BigDecimal valorTotalProduto) {
		this.valorTotalProduto = valorTotalProduto;
	}

	@Override
	public String toString() {
		return "FinContratoXCadProduto [codigo=" + codigo
				+ ", quantidadePedido=" + quantidadePedido
				+ ", valorTotalProduto=" + valorTotalProduto + "]";
	}
	
}
