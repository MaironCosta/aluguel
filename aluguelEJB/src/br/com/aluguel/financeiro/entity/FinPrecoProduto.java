package br.com.aluguel.financeiro.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.text.Collator;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.aluguel.cadastro.entity.CadFuncionario;
import br.com.aluguel.cadastro.entity.CadProduto;
import br.com.aluguel.interfaces.utils.IEntity;


@Entity
@Table(name = "FIN_PRECO_PRODUTO")
@SequenceGenerator(name="FIN_PRECO_PRODUTO_SEQUENCE",sequenceName="FIN_PRECO_PRODUTO_SEQ")
public class FinPrecoProduto implements IEntity, Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODIGO", precision = 18)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FIN_PRECO_PRODUTO_SEQUENCE")
	private Long codigo;
	
	@Column(name = "VALOR")
	private BigDecimal valor;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_INICIO")
	private Calendar dataInicio;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATA_FIM")
	private Calendar dataFim;
	
	@ManyToOne
	@JoinColumn(name = "FK_PRODUTO")
	private CadProduto produto;	

	@ManyToOne
	@JoinColumn(name = "FK_FUNCIONARIO_ALTERACAO")
	private CadFuncionario funcionarioAlteracao;
	
	public FinPrecoProduto() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getCodigo() {
		// TODO Auto-generated method stub
		return this.codigo;
	}

	@Override
	public void setCodigo(Long codigo) {
		// TODO Auto-generated method stub
		this.codigo = codigo;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public Calendar getDataInicio() {
		return dataInicio;
	}

	public void setDataInicio(Calendar dataInicio) {
		this.dataInicio = dataInicio;
	}

	public Calendar getDataFim() {
		return dataFim;
	}

	public void setDataFim(Calendar dataFim) {
		this.dataFim = dataFim;
	}

	public CadProduto getProduto() {
		return produto;
	}

	public void setProduto(CadProduto produto) {
		this.produto = produto;
	}

	public CadFuncionario getFuncionarioAlteracao() {
		return funcionarioAlteracao;
	}

	public void setFuncionarioAlteracao(CadFuncionario funcionarioAlteracao) {
		this.funcionarioAlteracao = funcionarioAlteracao;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result + ((dataFim == null) ? 0 : dataFim.hashCode());
		result = prime * result
				+ ((dataInicio == null) ? 0 : dataInicio.hashCode());
		result = prime
				* result
				+ ((funcionarioAlteracao == null) ? 0 : funcionarioAlteracao
						.hashCode());
		result = prime * result + ((produto == null) ? 0 : produto.hashCode());
		result = prime * result + ((valor == null) ? 0 : valor.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinPrecoProduto other = (FinPrecoProduto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (dataFim == null) {
			if (other.dataFim != null)
				return false;
		} else if (!dataFim.equals(other.dataFim))
			return false;
		if (dataInicio == null) {
			if (other.dataInicio != null)
				return false;
		} else if (!dataInicio.equals(other.dataInicio))
			return false;
		if (funcionarioAlteracao == null) {
			if (other.funcionarioAlteracao != null)
				return false;
		} else if (!funcionarioAlteracao.equals(other.funcionarioAlteracao))
			return false;
		if (produto == null) {
			if (other.produto != null)
				return false;
		} else if (!produto.equals(other.produto))
			return false;
		if (valor == null) {
			if (other.valor != null)
				return false;
		} else if (!valor.equals(other.valor))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FinPrecoProduto [codigo=" + codigo + ", valor=" + valor
				+ ", dataInicio=" + dataInicio + ", dataFim=" + dataFim
				+ ", produto=" + produto + ", funcionarioAlteracao="
				+ funcionarioAlteracao + "]";
	}

	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((FinPrecoProduto)o).getCodigo());
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (FinPrecoProduto) super.clone();
	}
}
