package br.com.aluguel.financeiro.entity;

import java.io.Serializable;
import java.text.Collator;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.aluguel.cadastro.entity.CadCliente;
import br.com.aluguel.cadastro.entity.CadFuncionario;
import br.com.aluguel.interfaces.utils.IEntity;

@Entity
@Table(name = "FIN_CONTRATO")
@SequenceGenerator(name="FIN_CONTRATO_SEQUENCE",sequenceName="FIN_CONTRATO_SEQ")
public class FinContrato implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@Id
	@Column(name="CODIGO", precision = 18)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="FIN_CONTRATO_SEQUENCE")
	private Long codigo;
	
	@ManyToOne
	@JoinColumn(name = "FK_FUNCIONARIO_ADMINISTRA")
	private CadFuncionario funcionarioAdministra;
	
	@ManyToOne
	@JoinColumn(name = "FK_CLIENTE")
	private CadCliente cliente;
	
	@Temporal(TemporalType.DATE)
	@Column(name = "DATA_INICIO_CONTRATO")
	private Calendar dataInicioContrato;

	@Temporal(TemporalType.DATE)
	@Column(name = "DATA_FIM_CONTRATO")
	private Calendar dataFimContrato;
	
	@Column(name = "NUMERO_CONTRATO")
	private String numeroContrato;

	public FinContrato() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getCodigo() {
		// TODO Auto-generated method stub
		return this.codigo;
	}

	@Override
	public void setCodigo(Long codigo) {
		// TODO Auto-generated method stub
		this.codigo = codigo;
	}

	public CadFuncionario getFuncionarioAdministra() {
		return funcionarioAdministra;
	}

	public void setFuncionarioAdministra(CadFuncionario funcionarioAdministra) {
		this.funcionarioAdministra = funcionarioAdministra;
	}

	public CadCliente getCliente() {
		return cliente;
	}

	public void setCliente(CadCliente cliente) {
		this.cliente = cliente;
	}

	public Calendar getDataInicioContrato() {
		return dataInicioContrato;
	}

	public void setDataInicioContrato(Calendar dataInicioContrato) {
		this.dataInicioContrato = dataInicioContrato;
	}

	public Calendar getDataFimContrato() {
		return dataFimContrato;
	}

	public void setDataFimContrato(Calendar dataFimContrato) {
		this.dataFimContrato = dataFimContrato;
	}

	public String getNumeroContrato() {
		return numeroContrato;
	}

	public void setNumeroContrato(String numeroContrato) {
		

		
		this.numeroContrato = numeroContrato;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cliente == null) ? 0 : cliente.hashCode());
		result = prime * result
				+ ((dataFimContrato == null) ? 0 : dataFimContrato.hashCode());
		result = prime
				* result
				+ ((dataInicioContrato == null) ? 0 : dataInicioContrato
						.hashCode());
		result = prime
				* result
				+ ((funcionarioAdministra == null) ? 0 : funcionarioAdministra
						.hashCode());
		result = prime * result
				+ ((numeroContrato == null) ? 0 : numeroContrato.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		FinContrato other = (FinContrato) obj;
		if (cliente == null) {
			if (other.cliente != null)
				return false;
		} else if (!cliente.equals(other.cliente))
			return false;
		if (dataFimContrato == null) {
			if (other.dataFimContrato != null)
				return false;
		} else if (!dataFimContrato.equals(other.dataFimContrato))
			return false;
		if (dataInicioContrato == null) {
			if (other.dataInicioContrato != null)
				return false;
		} else if (!dataInicioContrato.equals(other.dataInicioContrato))
			return false;
		if (funcionarioAdministra == null) {
			if (other.funcionarioAdministra != null)
				return false;
		} else if (!funcionarioAdministra.equals(other.funcionarioAdministra))
			return false;
		if (numeroContrato == null) {
			if (other.numeroContrato != null)
				return false;
		} else if (!numeroContrato.equals(other.numeroContrato))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "FinContrato [codigo=" + codigo + ", funcionarioAdministra="
				+ funcionarioAdministra + ", cliente=" + cliente
				+ ", dataInicioContrato=" + dataInicioContrato
				+ ", dataFimContrato=" + dataFimContrato + ", numeroContrato="
				+ numeroContrato + "]";
	}

	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((FinContrato)o).getCodigo());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (FinContrato) super.clone();
	}
}
