package br.com.aluguel.cadastro.entity;

import java.io.Serializable;
import java.text.Collator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.aluguel.interfaces.utils.IEntity;


@Entity
@Table(name = "CAD_ENDERECO")
@SequenceGenerator(name="CAD_ENDERECO_SEQUENCE",sequenceName="CAD_ENDERECO_SEQ")
public class CadEndereco implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODIGO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CAD_ENDERECO_SEQUENCE")
	private Long codigo;
	
	@Column(name = "CEP")
	private String cep;

	@Column(name = "MUNICIPIO")
	private String municipio;

	@Column(name = "BAIRRO")
	private String bairro;

	@Column(name = "LOGRADOURO")
	private String logradouro;
	
	@Column(name = "NUMERO")
	private String numero;

	@Column(name = "COMPLEMENTO")
	private String complemento;

	@JoinColumn(name = "UF")
	@ManyToOne()
	private CadUF uf;
	
	public CadEndereco() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getCodigo() {
		// TODO Auto-generated method stub
		return this.codigo;
	}

	@Override
	public void setCodigo(Long codigo) {
		// TODO Auto-generated method stub
		this.codigo = codigo;
	}

	public String getCep() {
		return this.cep;
	}

	public void setCeo(String cep) {
		this.cep = cep;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}
	
	public String getMunicipio() {
		return municipio;
	}

	public void setMunicipio(String municipio) {
		this.municipio = municipio;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public CadUF getUf() {
		return uf;
	}

	public void setUf(CadUF uf) {
		this.uf = uf;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((cep == null) ? 0 : cep.hashCode());
		result = prime * result + ((numero == null) ? 0 : numero.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadEndereco other = (CadEndereco) obj;
		if (cep == null) {
			if (other.cep != null)
				return false;
		} else if (!cep.equals(other.cep))
			return false;
		if (numero == null) {
			if (other.numero != null)
				return false;
		} else if (!numero.equals(other.numero))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CadEndereco [codigo=" + codigo + ", CEP=" + cep
				+ ", municipio=" + municipio + ", bairro=" + bairro
				+ ", logradouro=" + logradouro + ", numero=" + numero
				+ ", complemento=" + complemento + ", uf=" + uf + "]";
	}

	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((CadEndereco)o).getCodigo());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (CadEndereco) super.clone();
	}
}
