package br.com.aluguel.cadastro.entity;

import java.io.Serializable;
import java.text.Collator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.aluguel.interfaces.utils.IEntity;

@Entity
@Table(name = "CAD_PRODUTO")
@SequenceGenerator(name="CAD_PRODUTO_SEQUENCE",sequenceName="CAD_PRODUTO_SEQ")
public class CadProduto implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODIGO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CAD_PRODUTO_SEQUENCE")
	private Long codigo;

	@Column(name="ATIVO")
	private Boolean isAtivo;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@ManyToOne
	@JoinColumn(name = "FK_CODIGO_FORNECEDOR")
	private CadFornecedor fornecedor;
	
	@Lob
	@Column(name = "IMAGEM_PRODUTO")
	private byte[] imagemProduto;
	
	public CadProduto() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getCodigo() {
		// TODO Auto-generated method stub
		return this.codigo;
	}

	@Override
	public void setCodigo(Long codigo) {
		// TODO Auto-generated method stub
		this.codigo = codigo;
	}

	public Boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(Boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public CadFornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(CadFornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result
				+ ((descricao == null) ? 0 : descricao.hashCode());
		result = prime * result
				+ ((fornecedor == null) ? 0 : fornecedor.hashCode());
		result = prime * result + (isAtivo ? 1231 : 1237);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadProduto other = (CadProduto) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (descricao == null) {
			if (other.descricao != null)
				return false;
		} else if (!descricao.equals(other.descricao))
			return false;
		if (fornecedor == null) {
			if (other.fornecedor != null)
				return false;
		} else if (!fornecedor.equals(other.fornecedor))
			return false;
//		if (isAtivo != other.isAtivo)
//			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CadProduto [codigo=" + codigo + ", isAtivo=" + isAtivo
				+ ", descricao=" + descricao + ", fornecedor=" + fornecedor
				+ "]";
	}
	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((CadProduto)o).getCodigo());
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (CadProduto) super.clone();
	}
}
