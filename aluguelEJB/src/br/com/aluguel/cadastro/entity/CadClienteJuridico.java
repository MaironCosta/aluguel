package br.com.aluguel.cadastro.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="CAD_CLIENTE_JURIDICO")
@PrimaryKeyJoinColumn(name="CODIGO")
public class CadClienteJuridico extends CadCliente {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="FK_CODIGO_JURIDICA")
	private CadPessoaJuridica pessoaJuridica;

	public CadClienteJuridico() {
		// TODO Auto-generated constructor stub
	}

	public CadPessoaJuridica getPessoaJuridica() {
		return pessoaJuridica;
	}

	public void setPessoaJuridica(CadPessoaJuridica pessoaJuridica) {
		this.pessoaJuridica = pessoaJuridica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((pessoaJuridica == null) ? 0 : pessoaJuridica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadClienteJuridico other = (CadClienteJuridico) obj;
		if (pessoaJuridica == null) {
			if (other.pessoaJuridica != null)
				return false;
		} else if (!pessoaJuridica.equals(other.pessoaJuridica))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CadClienteJuridico [pessoaJuridica=" + pessoaJuridica + "]";
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (CadClienteJuridico) super.clone();
	}
	
}
