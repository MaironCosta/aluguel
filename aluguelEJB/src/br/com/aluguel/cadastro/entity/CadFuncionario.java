package br.com.aluguel.cadastro.entity;

import java.io.Serializable;
import java.text.Collator;
import java.util.Calendar;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import br.com.aluguel.interfaces.utils.IEntity;

@Entity
@Table(name = "CAD_FUNCIONARIO")
//@INHERITANCE(STRATEGY=INHERITANCETYPE.JOINED)
@SequenceGenerator(name="CAD_FUNCIONARIO_SEQUENCE",sequenceName="CAD_FUNCIONARIO_SEQ")
public class CadFuncionario implements IEntity, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name="CODIGO", precision = 18)
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CAD_FUNCIONARIO_SEQUENCE")
	private Long codigo;

	@Column(name="ATIVO")
	private Boolean isAtivo;

	@Column(name="REGISTRO")
	private String registro;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATACADASTRO")
	private Calendar dataCadastro;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "DATADESLIGAMENTO")
	private Calendar dataDesligamento;
	
	public CadFuncionario() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getCodigo() {
		// TODO Auto-generated method stub
		return codigo;
	}

	@Override
	public void setCodigo(Long codigo) {
		// TODO Auto-generated method stub
		this.codigo = codigo;
	}

	public Boolean isAtivo() {
		return isAtivo;
	}

	public void setAtivo(boolean isAtivo) {
		this.isAtivo = isAtivo;
	}

	public String getRegistro() {
		return registro;
	}

	public void setRegistro(String registro) {
		this.registro = registro;
	}

	public Calendar getDataCadastro() {
		return dataCadastro;
	}

	public void setDataCadastro(Calendar dataCadastro) {
		this.dataCadastro = dataCadastro;
	}

	public Calendar getDataDesligamento() {
		return dataDesligamento;
	}

	public void setDataDesligamento(Calendar dataDesligamento) {
		this.dataDesligamento = dataDesligamento;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((codigo == null) ? 0 : codigo.hashCode());
		result = prime * result
				+ ((registro == null) ? 0 : registro.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadFuncionario other = (CadFuncionario) obj;
		if (codigo == null) {
			if (other.codigo != null)
				return false;
		} else if (!codigo.equals(other.codigo))
			return false;
		if (registro == null) {
			if (other.registro != null)
				return false;
		} else if (!registro.equals(other.registro))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "CadFuncionario [codigo=" + codigo + ", isAtivo=" + isAtivo
				+ ", registro=" + registro + ", dataCadastro=" + dataCadastro
				+ ", dataDesligamento=" + dataDesligamento + "]";
	}
	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((CadFuncionario)o).getCodigo());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (CadFuncionario) super.clone();
	}
}
