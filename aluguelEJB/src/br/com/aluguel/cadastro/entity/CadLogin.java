package br.com.aluguel.cadastro.entity;

import java.text.Collator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.aluguel.interfaces.utils.IEntity;

@Entity
@Table(name = "CAD_LOGIN")
@SequenceGenerator(name="CAD_LOGIN_SEQUENCE",sequenceName="CAD_LOGIN_SEQ")
public class CadLogin implements IEntity {
	
	@Id
	@Column(name="CODIGO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="CAD_LOGIN_SEQUENCE")
	private Long codigo;
	
	@Column(name="SENHA")
	private String senha;
	
	@ManyToOne
	@JoinColumn(name = "FK_CODIGO_FUNCIONARIO")
	private CadFuncionario funcionario;

	@ManyToOne
	@JoinColumn(name = "FK_CODIGO_CLIENTE")
	private CadCliente cliente;

	@ManyToOne
	@JoinColumn(name = "FK_CODIGO_FORNECEDOR")
	private CadFornecedor fornecedor;
	
	public CadLogin() {
		// TODO Auto-generated constructor stub
	}

	@Override
	public Long getCodigo() {
		// TODO Auto-generated method stub
		return this.codigo;
	}

	@Override
	public void setCodigo(Long codigo) {
		// TODO Auto-generated method stub
		this.codigo = codigo;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public CadFuncionario getFuncionario() {
		return funcionario;
	}

	public void setFuncionario(CadFuncionario funcionario) {
		this.funcionario = funcionario;
	}

	public CadCliente getCliente() {
		return cliente;
	}

	public void setCliente(CadCliente cliente) {
		this.cliente = cliente;
	}

	public CadFornecedor getFornecedor() {
		return fornecedor;
	}

	public void setFornecedor(CadFornecedor fornecedor) {
		this.fornecedor = fornecedor;
	}

	@Override
	public String toString() {
		return "CadLogin [codigo=" + codigo + ", senha=" + senha
				+ ", funcionario=" + funcionario + ", cliente=" + cliente
				+ ", fornecedor=" + fornecedor + "]";
	}
	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((CadLogin)o).getCodigo());
	}
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (CadLogin) super.clone();
	}
}
