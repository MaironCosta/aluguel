package br.com.aluguel.cadastro.entity;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;

@Entity
@Table(name="CAD_FORNECEDOR_FISICO")
@PrimaryKeyJoinColumn(name="CODIGO")
public class CadFornecedorFisico extends CadFornecedor {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	@ManyToOne
	@JoinColumn(name="FK_CODIGO_PESSOA")
	private CadPessoaFisica pessoaFisica;
	
	public CadFornecedorFisico() {
		// TODO Auto-generated constructor stub
	}

	public CadPessoaFisica getPessoaFisica() {
		return pessoaFisica;
	}

	public void setPessoaFisica(CadPessoaFisica pessoaFisica) {
		this.pessoaFisica = pessoaFisica;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result
				+ ((pessoaFisica == null) ? 0 : pessoaFisica.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		CadFornecedorFisico other = (CadFornecedorFisico) obj;
		if (pessoaFisica == null) {
			if (other.pessoaFisica != null)
				return false;
		} else if (!pessoaFisica.equals(other.pessoaFisica))
			return false;
		return true;
	}

	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (CadFornecedorFisico) super.clone();
	}
}
