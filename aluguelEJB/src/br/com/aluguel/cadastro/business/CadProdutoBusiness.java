package br.com.aluguel.cadastro.business;

import java.util.Collection;

import br.com.aluguel.business.SuperBusiness;
import br.com.aluguel.cadastro.entity.CadProduto;
import br.com.aluguel.exception.ValidacaoException;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class CadProdutoBusiness
 */
@Stateless
@LocalBean
public class CadProdutoBusiness extends SuperBusiness implements CadProdutoBusinessFacade {
       
    /**
     * @see SuperBusiness#SuperBusiness()
     */
    public CadProdutoBusiness() {
        super();
        // TODO Auto-generated constructor stub
    }

	@Override
	public CadProduto persist(CadProduto obj) throws ValidacaoException,
			IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void delete(CadProduto obj) throws ValidacaoException,
			IllegalArgumentException {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Collection<CadProduto> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public CadProduto findByCod(long codigo) throws ValidacaoException,
			IllegalArgumentException {
		// TODO Auto-generated method stub
		return null;
	}

}
