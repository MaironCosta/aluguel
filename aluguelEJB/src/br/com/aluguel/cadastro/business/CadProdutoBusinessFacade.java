package br.com.aluguel.cadastro.business;

import javax.ejb.Local;

import br.com.aluguel.business.IBusinessFacade;
import br.com.aluguel.cadastro.entity.CadProduto;

@Local
public interface CadProdutoBusinessFacade extends IBusinessFacade<CadProduto> {

}
