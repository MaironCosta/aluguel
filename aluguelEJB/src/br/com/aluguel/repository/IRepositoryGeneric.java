package br.com.aluguel.repository;

import java.util.Collection;


public interface IRepositoryGeneric<T> {

	Object persistence (T t);
	Object merge (T t);
	void remove (T t);
	Collection<T> findAll (Class<?> classe);
	public Collection<Object> findAll(Class<?> classe, boolean isAtivo);
	public Object findByField(String fieldName, Class<?> classe, Object value);
	public Collection<Object> findByField(Class<?> classe, Object value, String fieldName);
	public Collection<Object> findByLikeField(Class<?> classe, String fieldName, String value);	

}
