package br.com.aluguel.repository;

import java.io.Serializable;
import java.util.Collection;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.aluguel.cadastro.entity.CadLogin;
import br.com.aluguel.interfaces.utils.IEntity;

//@Stateless
@SuppressWarnings({"unchecked"})
public class RepositoryGeneric implements Serializable, IRepositoryGeneric<Object> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7086859252950189103L;
	
//	Logger log = Logger.getLogger(RepositoryGeneric.class);	
	
	private final EntityManager entityManager;	
		
	public RepositoryGeneric(EntityManager entityManager) {
		// TODO Auto-generated constructor stub
		
		super();
		this.entityManager = entityManager;
				
	}
	
	public EntityManager getEntityManager() {
		
		return this.entityManager;
	}

	@Override
	public Object persistence(Object t) {
		// TODO Auto-generated method stub
				
		try {
						
			this.getEntityManager().persist(t);
			this.getEntityManager().flush();
			this.getEntityManager().refresh(t);
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return t;
	}

	@Override
	public Object merge(Object t) {
		// TODO Auto-generated method stub
		
		try {
			
			this.getEntityManager().merge(t);
			this.getEntityManager().flush();
			this.getEntityManager().refresh(t);
			
		} catch (Exception e) {
			// TODO: handle exception
		
			e.printStackTrace();
		
		}
		
		return t;
	}

	@Override
	public void remove(Object t) {
		// TODO Auto-generated method stub

		try {
			
		//	entityManager.evict(t);
			Query query = this.getEntityManager().createNamedQuery("DELETE FROM " + t.getClass().getSimpleName().toUpperCase() + " WHERE codigo = " + ((IEntity)t).getCodigo());
			query.executeUpdate();
		//	session.flush();
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	@Override
	public Collection<Object> findAll(Class<?> classe) {
		// TODO Auto-generated method stub

		Collection<Object> result = null;
		
		try {
			
			Query query = this.getEntityManager().createQuery("FROM " + classe.getName());			
			result = query.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
			// // log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Collection<Object> findAll(Class<?> classe, boolean isAtivo) {
		// TODO Auto-generated method stub

		Collection<Object> result = null;
		
		try {
			
			Query query = this.getEntityManager().createQuery("FROM " + classe.getName() + " WHERE isAtivo is " + isAtivo);			
			result = query.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}
	
	@Override
	public Object findByField(String fieldName, Class<?> classe, Object value){
		
		Object result = null;
		
		try {
			
			Query query = this.getEntityManager().createQuery("from " + classe.getName() + " x WHERE x." + fieldName + "= :value");
			query.setParameter("value", value);
			query.setMaxResults(1);
			
			result = query.getSingleResult();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Collection<Object> findByField(Class<?> classe, Object value, String fieldName){
		
		Collection<Object> result = null;
		
		try {
			Query query = this.getEntityManager().createQuery("from " + classe.getName() + " x WHERE x." + fieldName + " = :value");
			query.setParameter("value", value);
			
			result = query.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	@Override
	public Collection<Object> findByLikeField(Class<?> classe, String fieldName, String value){
		
		Collection<Object> result = null;
		
		try {
			Query query = this.getEntityManager().createQuery("FROM " + classe.getName() + " x WHERE x." + fieldName + " like :value");
			query.setParameter("value", "%" + value + "%");
			
			result = query.getResultList();
			
		} catch (Exception e) {
			// TODO: handle exception
			// log.info(e.getMessage());
			e.printStackTrace();
		}
		
		return result;
	}

	public static void main(String[] args) {
		
		CadLogin login = new CadLogin();
		login.setCodigo(1l);
		System.out.println(login.getClass().getName());
		
		Object o = login;
		System.out.println(o.getClass().getSimpleName());
		
		IEntity entity = (IEntity) o;
		System.out.println(entity.getClass().getName() + " " + entity.getCodigo());
		
		
	}
	
}
