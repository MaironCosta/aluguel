package br.com.aluguel.business;

import java.util.ArrayList;
import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import br.com.aluguel.cadastro.entity.CadPessoa;
import br.com.aluguel.exception.ValidacaoException;
import br.com.aluguel.interfaces.utils.IEntity;
import br.com.aluguel.repository.IRepositoryGeneric;
import br.com.aluguel.repository.RepositoryGeneric;

@Stateless
public class SuperBusiness {

//	Logger log = Logger.getLogger(SuperBusiness.class);	
	
	private List<String> msg = new ArrayList<String>();	

	@PersistenceContext(unitName = "aluguelJPA")
	private EntityManager entityManager;
	
//	@Inject
//	private Connections connections;	
	
	private IRepositoryGeneric<Object> repositoryGeneric;
				
	public SuperBusiness() {
		// TODO Auto-generated constructor stub
	}
	
	public List<String> getMsg() {
		return msg;
	}

	public void setMsg(List<String> msg) {
		this.msg = msg;
	}
	
	public EntityManager getConection_ALUGUEL(){
		System.out.println("Abrindo Conexao");
			
		return entityManager;
//		return Connections.getEntityManager();
	}
	
	public IEntity createUpdate(IEntity obj) throws ValidacaoException {
		// TODO Auto-generated method stub
				
	//	IRepositoryGeneric<Object> repositoryGeneric = new RepositoryGeneric();	

		repositoryGeneric = new RepositoryGeneric(this.getConection_ALUGUEL());
		
		try {
			
		//	getConection_SISGED().persist(obj);
			obj = (IEntity) ((obj.getCodigo() == null)?repositoryGeneric.persistence(obj):repositoryGeneric.merge(obj));
			
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		//	transaction.rollback();
		} finally {
			
		//	this.fecharConexoes(session);
			
		}
		
		return (IEntity) obj;
	}
	
	public static void main(String[] args) {
		
		SuperBusiness sb = new SuperBusiness();
		CadPessoa p = new CadPessoa();
		p.setEmail("teste@ejb.com");
		
//		EntityManager entityManager = getConection_SISGED();
		
		/*try {
			System.out.println(sb.createUpdate(p, entityManager));
		} catch (ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}*/
		
		
	}
	
}
