package br.com.aluguel.business;

import java.util.Collection;

import javax.ejb.Local;

import br.com.aluguel.exception.ValidacaoException;

@Local
public interface IBusinessFacade<T> {
	
//	public void iniciar();
	public T persist (T obj) throws ValidacaoException, IllegalArgumentException;	
	public void delete (T obj) throws ValidacaoException, IllegalArgumentException;
	public Collection<T> findAll();
	public T findByCod(long codigo) throws ValidacaoException, IllegalArgumentException;	
	
}
