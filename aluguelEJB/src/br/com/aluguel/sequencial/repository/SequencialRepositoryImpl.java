package br.com.aluguel.sequencial.repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.aluguel.repository.RepositoryGeneric;
import br.com.aluguel.sequencial.entity.Sequencial;
import br.com.aluguel.sequencial.entity.TipoSequencial;

//@Stateless
public class SequencialRepositoryImpl extends RepositoryGeneric implements SequencialRepository {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
//	private final EntityManager entityManager;

	public SequencialRepositoryImpl(EntityManager entityManager) {
		super(entityManager);
		// TODO Auto-generated constructor stub
//		this.entityManager = entityManager;
	}
	
	@Override
	public Sequencial getBy(int ano, TipoSequencial tipoSequencial) {
		// TODO Auto-generated method stub
		
		Sequencial sequencial = null;
		
		StringBuilder completeQuery = new StringBuilder();
		completeQuery.append(" FROM Sequencial s WHERE ");
		completeQuery.append(" s.ano = :ano ");
		completeQuery.append(" AND s.tipoSequencial = :tipoSequencial");
		completeQuery.append(" ORDER BY s.ano ");
		
		Query query = super.getEntityManager().createQuery(completeQuery.toString());
		query.setParameter("ano", ano);
		query.setParameter("tipoSequencial", tipoSequencial.getTipoSequencial());
		query.setMaxResults(1);
		
		try {
			sequencial = (Sequencial) query.getSingleResult();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			
			if (!(e instanceof NoResultException)) {

				e.printStackTrace();
			}
		}
		
		return sequencial;
	}

}
