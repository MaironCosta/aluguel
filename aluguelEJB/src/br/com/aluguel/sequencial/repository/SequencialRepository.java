package br.com.aluguel.sequencial.repository;

import br.com.aluguel.repository.IRepositoryGeneric;
import br.com.aluguel.sequencial.entity.Sequencial;
import br.com.aluguel.sequencial.entity.TipoSequencial;

//@Local
public interface SequencialRepository extends IRepositoryGeneric<Object> {

	Sequencial getBy(int ano, TipoSequencial tipoSequencial);
	
}
