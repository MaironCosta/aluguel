package br.com.aluguel.sequencial.entity;

import java.text.Collator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

import br.com.aluguel.interfaces.utils.IEntity;

@Entity
@Table(name = "SEQ_SEQUENCIAL")
@SequenceGenerator(name="SEQ_SEQUENCIAL_SEQUENCE",sequenceName="SEQ_SEQUENCIAL_SEQ")
public class Sequencial implements IEntity {

	@Id
	@Column(name="CODIGO")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="SEQ_SEQUENCIAL_SEQUENCE")
	private Long codigo;
	
	// 1, 2, 3 - quantidade da sequencia
	@Column(name = "SEQUENCIAL")
	private long sequencial;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	// 2014, 2015, 2016
	@Column(name = "ANO")
	private int ano;
	
	//
	@Column(name = "TIPO_SEQUENCIAL")
	private int tipoSequencial;
	
	public Sequencial() {
		// TODO Auto-generated constructor stub
	}
	
	public Sequencial(long sequencial, int ano, TipoSequencial tipoSequencial) {
		super();
		this.sequencial = sequencial;
		this.descricao = tipoSequencial.getDescricao();
		this.ano = ano;
		this.tipoSequencial = tipoSequencial.getTipoSequencial();
	}

	public Long getCodigo() {
		return codigo;
	}

	public void setCodigo(Long codigo) {
		this.codigo = codigo;
	}

	public long getSequencial() {
		return sequencial;
	}

	public void setSequencial(long sequencial) {
		this.sequencial = sequencial;
	}

	public String getDescricao() {
		return descricao;
	}
/*
	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}*/

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public int getTipoSequencial() {
		return tipoSequencial;
	}

/*	public void setTipoSequencial(int tipoSequencial) {
		this.tipoSequencial = tipoSequencial;
	}*/

	public void setTipoSequencial(TipoSequencial tipoSequencial) {
		this.tipoSequencial = tipoSequencial.getTipoSequencial();
	}

	@Override
	public String toString() {
		return "Sequencial [codigo=" + codigo + ", sequencial=" + sequencial
				+ ", descricao=" + descricao + ", ano=" + ano
				+ ", tipoSequencial=" + tipoSequencial + "]";
	}

	@Override
	public int compareTo(IEntity o) {
		// TODO Auto-generated method stub
		return Collator.getInstance().compare(codigo, ((Sequencial)o).getCodigo());
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return (Sequencial)super.clone();
	}
	
}
