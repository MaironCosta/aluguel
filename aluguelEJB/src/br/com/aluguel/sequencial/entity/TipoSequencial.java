package br.com.aluguel.sequencial.entity;

public enum TipoSequencial {

	NUMEROCONTRATO (1, "NUMERO CONTRATO"),
	REGISTROCLIENTEFISICO (2, "REGISTRO CLIENTE FISICO"),
	REGISTROCLIENTEJURIDICO (3, "REGISTRO CLIENTE JURIDICO"),
	REGISTROFUNCIONARIO (4, "REGISTRO FUNCIONARIO"),
	REGISTROFORNECEDOR (6, "REGISTRO FORNECEDOR");
	
	private int tipoSequencial;
	private String descricao;
	
	public int getTipoSequencial(){
		return this.tipoSequencial;
	}
	
	public String getDescricao() {
		return this.descricao;
	}
	
	private TipoSequencial (int tipoSequencial, String descricao) {
		this.tipoSequencial = tipoSequencial;
		this.descricao = descricao;
	}
	
}
