package br.com.aluguel.sequencial.business;

import javax.ejb.Local;

import br.com.aluguel.business.IBusinessFacade;
import br.com.aluguel.exception.ValidacaoException;
import br.com.aluguel.sequencial.entity.Sequencial;
import br.com.aluguel.sequencial.entity.TipoSequencial;

@Local
public interface SequencialBusinessFacade extends IBusinessFacade<Sequencial> {

	Sequencial getBy(int ano, TipoSequencial tipoSequencial) throws ValidacaoException, IllegalArgumentException;
	
}
