package br.com.aluguel.sequencial.business;

import java.util.Calendar;
import java.util.Collection;

import javax.ejb.Local;
import javax.ejb.Stateless;

import br.com.aluguel.business.SuperBusiness;
import br.com.aluguel.cadastro.repository.CadProdutoRepository;
import br.com.aluguel.cadastro.repository.CadProdutoRepositoryImpl;
import br.com.aluguel.exception.ValidacaoException;
import br.com.aluguel.sequencial.entity.Sequencial;
import br.com.aluguel.sequencial.entity.TipoSequencial;
import br.com.aluguel.sequencial.repository.SequencialRepository;
import br.com.aluguel.sequencial.repository.SequencialRepositoryImpl;
import br.com.aluguel.utils.ZoneIdLocales;

@Stateless
@Local
public class SequencialBusiness extends SuperBusiness implements SequencialBusinessFacade {
	
	private SequencialRepository sequencialRepository;
	private CadProdutoRepository produtoRepository;
	
	public SequencialBusiness() {
		// TODO Auto-generated constructor stub
		
	}
	
	public void iniciar() {
		// TODO Auto-generated method stub
		
		sequencialRepository = new SequencialRepositoryImpl(this.getConection_ALUGUEL());
		produtoRepository = new CadProdutoRepositoryImpl(this.getConection_ALUGUEL());

	}

	@Override
	public Sequencial persist(Sequencial obj) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		super.createUpdate(obj);
		
		return obj;
	}

	@Override
	public void delete(Sequencial obj) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub

	}

	@Override
	public Collection<Sequencial> findAll() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public Sequencial findByCod(long codigo) throws ValidacaoException {
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public Sequencial getBy(int ano, TipoSequencial tipoSequencial) throws ValidacaoException, IllegalArgumentException {
		// TODO Auto-generated method stub
		
		if (tipoSequencial == null) {
			
			throw new IllegalArgumentException("Informe o tipo sequencial.");
			
		}
			
		this.iniciar();
		
		Sequencial sequencial = sequencialRepository.getBy(ano, tipoSequencial);
		
		if (sequencial == null) {
			
			sequencial = new Sequencial(0, 
					                    Calendar.getInstance(ZoneIdLocales.LOCALE_ID_SAO_PAULO_BR).get(Calendar.YEAR), 
					                    tipoSequencial);
			
			sequencial = this.persist(sequencial);
		}
		
		return sequencial;
	}

}
