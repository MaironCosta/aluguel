package br.com.aluguel.teste;

import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;

import br.com.aluguel.cadastro.entity.CadPessoaFisica;
import br.com.aluguel.exception.ValidacaoException;
import br.com.aluguel.sequencial.business.SequencialBusiness;
import br.com.aluguel.sequencial.business.SequencialBusinessFacade;
import br.com.aluguel.sequencial.entity.Sequencial;
import br.com.aluguel.sequencial.entity.TipoSequencial;

public class Teste {

	public Teste() {
		// TODO Auto-generated constructor stub
	}
	
	public static void datasJava8 () {		
		
		CadPessoaFisica pessoaFisica = new CadPessoaFisica();
		
//		pessoaFisica.setDataNascimento(ZonedDateTime.now(ZoneIdLocales.ZONE_ID_SAO_PAULO_BR));
		
	//	LocalDate localDate = LocalDate.now(ZoneId.of("BET"));
		
	/*	for (Map.Entry<String, String> p : ZoneId.SHORT_IDS.entrySet()) {
			System.out.println("p.getKey(): " + p.getKey() + "   p.getValue(): " + p.getValue());
		}*/
		
		System.out.println(pessoaFisica.getDataNascimento());		
	
	}
	
	public static void idadeJava8 () {		
		
		LocalDate dtNasc = LocalDate.of(1987, 8, 20);
		Period period = Period.between(dtNasc, LocalDate.now());
		
		System.out.println(period.getYears());
		
		System.out.println(new CadPessoaFisica().getIdade());
			
	}
	
	public static void cadastrarSequencial(){
		
		SequencialBusinessFacade sequencialBusiness = new SequencialBusiness();
		
		int ano = Calendar.getInstance().get(Calendar.YEAR);
		Sequencial sequencial = new Sequencial(0L, 
				                               ano, 
				                               TipoSequencial.REGISTROFUNCIONARIO);
		
		try {
			
			sequencialBusiness.persist(sequencial);
			
		} catch (IllegalArgumentException | ValidacaoException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void main(String[] args) {

			cadastrarSequencial();
		//	datasJava8();
		//	idadeJava8();
		
	}
	
}
