package br.com.aluguel.interfaces.utils;

public interface IEntity extends Cloneable, Comparable<IEntity> {

	public Long getCodigo();
	public void setCodigo(Long codigo);
	public Object clone() throws CloneNotSupportedException;
	public int compareTo(IEntity o);
	
}
